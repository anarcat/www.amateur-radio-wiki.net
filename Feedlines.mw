Otherwise known as transmission lines, feedlines are used to transfer [[RF energy]] between a [[Transmitter]], [[Reciever]] or [[Transceivers|transceiver]] and the [[Antenna|antenna]].

= Types =

Three types of feed lines are used by amateurs : Coax, Balanced Line, and Waveguide. 

== Coaxial Cable ==

'''[[Coaxial Cable]]''' is made of a central conductor surrounded by a layer of insulating material with a second layer of conducting material over that and insulation on the outside. The inner conductor is either solid copper or stranded copper wire, while the outer conductor is either braided wire or a copper sheath.  A protective coating covers the conductors. '''Hardline''' is coaxial cable in which the outer conductor is solid copper. Most coaxial cables used in amateur installations have impedance of either 50 ohms of 75 ohms. The outer conductor of coaxial cable serves to confine electromagnetic energy within the cable.  Common grades of coax include [[RG-174]], [[RG-58]], RG-59, [[RG-8]], [[RG-213]], and [[9913]].

===Coaxial Connectors===

The most conmmon connectorss used in coaxial installatons are
* ''' Belling Lee''' [http://www.fmdx.info/index.php?id=wiring_belling_lee_plugs  Wiring a Belling Lee connector]
* '''BNC''' [http://www.insulatorseal.com/searchs/doc/WI-Con-BNC.htm  Wiring a BNC connector]
* '''F '''
* '''N-Type''' 
* '''UHF type'''

===Attenuation (Loss) in coaxial feedlines===

All feedlines exhibit some loss - essentially power reduction - especially over long distances. These losses are more critical, and greater, and at the highest frequency ranges.

This page : [[Coaxial loss table]] gives some information on expected coaxial feedline loss.

== Balanced Line ==

'''[[Balanced Line]]''' feedlines are composed of two conductors parallel to each other and a constant, fixed distance apart. Flat '''TV twinlead''' has an impedance of 300 ohms.  Amateurs more commonly use '''ladder line''', which has rectangular sections removed from the center insulator at regular intervals; ladder line typically has an impedance of either 300 or 450 ohms.  Both of these types of line are very sensitive to moisture (rain will change the impedance) and can interact with nearby objects (unlike coax, they should not be coiled or allowed to lie directly on the ground).  Another type is '''open wire''', which consists of two separate wires held a fixed distance apart by separate insulators.  Open wire line requires significant labor to set up, but it is impervious to precipitation and can be made in a range of impedances ranging from 300 to 600 ohms.

== Waveguides ==

'''[[Waveguide|Waveguides]]''' are used in microwave stations. A waveguide is a hollow tube made of conducting material, typically copper, through which electromagnetic energy is transmitted. The tube serves the same purpose as the outer conductor of coaxial cable, confining electromagnetic energy and directing it along the length of the waveguide.  Waveguides are not practical below microwave frequencies.

== External links==
* [http://www.ocarc.ca/coax.htm  Coaxial line loss calculator] From the Orchard City Amateur Radio Group
* [http://www.vk1od.net/tl/tllc.php Line Loss Calculator] from VK1OD

== Related pages ==

* [[Coaxial Cable]]
* [[Antennas]]
* [[Gain]]

{{antennas}}
