Related wiki page: [[Feedlines]], [[Capacity Hats]], [[Decibels]], [[Antenna Design]], [[Gain]], [[Bands]], [[SWR]], [[harmonics]], [[Front-to-back ratio]] [[Electromagnetic Waves]]

==Power Ratio dBm or dBmW ==

The power compared to a 1 milliwatt (1mW) source, expressed in decibels ([[Decibels| dB]])

Formula:

<math>
x = 10 \log_{10}(P/1 \ \mathrm{mW}) \,
</math>

Where P is the power in watts (W)

This roughly equates to:
*Double the power = 3dB increase in dBm
*Halve the power = 3dB decrease in dBm

{| class="wikitable" style="text-align: right;"
!dBm level !! Power 
|-
|80 dBm || 100 kW 
|-
|60 dBm || 1 kW = 1000 W 
|- 
|50 dBm || 100 W 
|- 
|40 dBm || 10 W 
|-
|36 dBm || 4 W 
|-
|33 dBm || 2 W 
|-
|30 dBm || 1 W = 1000 mW 
|-
|27 dBm || 500 mW 
|-
|26 dBm || 400 mW  
|-
|25 dBm || 316 mW
|-
|24 dBm || 250 mW 
|-
|23 dBm || 200 mW
|-
|22 dBm || 160 mW
|-
|21 dBm || 125 mW 
|-
|20 dBm || 100 mW 
|-
|15 dBm || 32 mW 
|-
|10 dBm || 10 mW
|-
| 6 dBm || 4.0 mW
|-
| 5 dBm || 3.2 mW
|-
| 4 dBm || 2.5 mW 
|-
| 3 dBm || 2.0 mW 
|-
| 2 dBm || 1.6 mW
|-
| 1 dBm || 1.3 mW
|-
| 0 dBm || 1.0 mW = 1000 µW 
|-
| −1 dBm || 794 µW
|-
| −3 dBm || 501 µW
|-
| −5 dBm || 316 µW
|-
| −10 dBm || 100 µW 
|-
| −20 dBm || 10 µW
|-
| −30 dBm || 1.0 µW = 1000 nW
|-
| −40 dBm || 100 nW
|-
| −50 dBm || 10 nW
|-
| −60 dBm || 1.0 nW = 1000 pW
|-
| −70 dBm || 100 pW 
|-
| −80 dBm || 10 pW
|-
| −100 dBm || 0.1 pW
|-
| −111 dBm || 0.008 pW = 8 fW 
|-
| −127.5 dBm || 0.178 fW = 178 aW 
|-
| −174 dBm || 0.004 aW 
|-
| −192.5 dBm || 0.00004 aW  
|-
| −∞ dBm   ||   0 W 
|}


more information can be found [http://en.wikipedia.org/wiki/DBm here]


{{electronics}}
