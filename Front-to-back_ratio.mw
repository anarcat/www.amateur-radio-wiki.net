Related wiki pages: [[Antennas]], [[Gain]], [[Propagation]], [[SWR]]

Front-to-back ratio (FBR) is a measure of how effectively a directional antennas, eg [[Yagi]], [[Log Periodic (LPDA)]], [[Dish or Parabola]] projects RF energy in the desired direction.

It is calculated as the ratio of the maximum directivity of the antenna to the directivity in the opposite direction.

To quantify FBR, a radiation pattern plot is drawn. 

FBR = maximum radiation (dB) in the forward direction - maximum radiation (dB) at <math> 180^0 </math>

FBR is of course meaningless for omnidirectional antennas.


{{antennas}}
