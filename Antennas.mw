{{Template:Stub}}

Antennas are electrical circuits designed to facilitate the transmission and/or reception of electromagnetic radiation.

Antennas are specifically designed to transmit/receive as much electromagnetic radiation as possible, whereas most circuits are designed to emit/detect as little as possible.  However, even a [[dummy load]] will emit a small amount of electromagnetic energy when radio-frequency oscillations are applied to it.

== Antenna Size ==

Optimized antennas will have dimensions of the same order as the wavelength of operation.  A [[balanced antenna]] will typically be about a half-wavelength long, and an [[unbalanced antenna]] such as a [[vertical]] will typically be about a quarter-wavelength long.  It is possible to shorten antennas drastically at the cost of efficiency.  See [[antenna loading]] for more discussion.

== Theory of Antennas ==

In terms of their construction, antennas are RLC circuits in which [[resistors|resistance]], [[inductors|inductance]] and [[capacitors|capacitance]] are distributed along a conductor, rather than being concentrated in a particular component such as an inductance in an inductor.

The "ideal" antenna will be [[Resonance|resonant]] for the frequency it is used at. In practical terms, this is impossible unless only a limited number of frequencies are used all the time.

Hence antenna design is a compromise, but good results can be achieved if we understand the notion of [[Harmonics|harmonics]].

== Antenna Types ==

'''Simple Antennas:'''
* [[Dipole]]
* [[Inverted-L antenna | Inverted-L]]
* [[Loop antenna|Loop]]
* [[Omnidirectional Antenna|Omnidirectional]]
* [[Random Wire Antenna|Random Wire]]
* [[Vertical Antenna|Vertical]]
* [[Wire Antenna|Wire]]

'''Complex Antennas:'''
* [[Beam]]
* [[DDRR]]
* [[Dish or Parabola]]
* [[Log Periodic (LPDA)]] 
* [[Quad antenna|Quad]]
* [[Yagi antenna|Yagi]]

'''Specialized Antennas:'''
* [[DF - Direction Finding]]
* [[Mobile antenna|Mobile]]
* [[Panel antenna | Panel]]
* [[Portable antenna|Portable]]

== Baluns ==

A balun is a transformer that converts a signal that is balanced with respect to ground - equal currents in opposite directions - to one that is unbalanced with respect to ground - one conductor and a ground. baluns can take a number of forms, but all types utilise electromagetic coupling in their operation.  In ham radio, baluns are often used to change impedance - for example the Guanella balun can be used to converts a 300Ohm line to a 75Ohm line.

==Understanding Antennas==

[http://www.hamuniverse.com/n4jaantennabook.html UNDERSTANDING ANTENNAS FOR THE NON-TECHNICAL HAM]

== Parts for antenna construction ==

Please go to these wiki pages [[Suppliers]]  [[Towers and Masts]]

== See also ==

* [[Feedlines]]
* [[Capacity Hats]]
* [[Antenna Design]]
* [[Gain]]
* [[Bands]]
* [[SWR]]
* [[harmonics]]
* [[Front-to-back ratio]]
* [[Electromagnetic Waves]]
* [[Radiated Power Measurement]]

{{antennas}}
