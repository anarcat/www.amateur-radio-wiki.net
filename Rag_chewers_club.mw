The '''Rag Chewers Club''', traditionally an introductory [[Awards and Certificates|amateur radio award]], was issued to the operators of thousands of amateur radio stations and many long-time operators can proudly claim to have received it as their first award in the 1950's and 1960's.

----

<center><big>Novice, Technician or Extra--start off right, where just about every awards hunter begins, with the Rag Chewers Club certificate, which encourages friendly, meaningful contacts, rather than the impersonal hello-goodbye QSO.  "Chew the rag" for at least 1/2 hour, and report that QSO to HQ, and enclose a business-size SASE.  Your Rag Chewers Club certificate will be sent to you by return mail.  Your first contact as a licensed amateur could earn your first award!</big><sup>[http://www.cnunix.com/ftp/hamradio/oak.oakland/hamradio/arrl/bbs/general/awards.txt]</sup></center>

----

Originally available to [[ARRL]] members for the price of a confirmed half-hour QSO and a self-addressed stamped envelope, and later for a $10 fee, the award was issued by ARRL headquarters until 2004. As the cost of issuing the awards continued to increase, the number of applications for many of the common introductory radio amateur honours began to decline.

__TOC__
:"One of the most popular ham activities is simply chatting about nothing in particular. In other words, chewing the fat, or in ham jargon, 'Rag Chewing'. Based on this popular activity, it seems only right that rag chewing should be acknowledged as a truly ham activity. For anyone not familiar with the term, rag chewing isn't just contacting someone. It involves actually conversing, talking and listening for more than a few minutes."<sup>[http://www.spar-hams.org/index.php?pg=11]</sup>

== Demise and relaunch ==
On Feb 12, 2004 three long-time ARRL awards were discontinued: "Effective immediately, the ARRL Awards Branch has discontinued the Rag Chewer's Club, the Old Timer's Club and the Friendship Award. ARRL Membership Services Manager Wayne Mills, N7NG, says that the number of amateurs applying for awards in general has declined significantly over the years, and interest in these three awards had slowed to a trickle. DXCC and WAS remain among the most popular ARRL Awards, he said, but the eliminated awards 'had outlived their interest level.'"

Of the three, two (RCC and Friendship Award) were awards a new ARRL member could easily earn.

;ARRL Friendship Award: "The purpose of this award is to encourage friendly contact between radio amateurs (hams) and thereby discover new friends through personal communication with others.  The ARRL Friendship Award is available to any ARRL member who submits log extracts that show two-way communications with 26 stations whose callsigns end with each of the 26 letters of the alphabet.  (For example:  W4RA, K0ORB, W3ABC...K1ZZ.)"

The third was an award for operators who had first been on-air twenty or more years ago:<sup>[http://www.enter.net/~wxdata/timer.htm]</sup>

;Old Timer's Club:"The coveted OTC certificate can be yours if you have been licensed at least 20 years.  Drop a note to HQ with the date of your first license, and your call (both then and now), along with a business-size SASE.  HQ will verify this information and send your Old Timers Club certificate promptly."

=== Society for the Preservation of Amateur Radio ===

The Rag Chewers Club award was promptly re-launched by the [[Society for the Preservation of Amateur Radio]] (SPAR), which announced that as of September 1, 2004 that it would administer this award following the old ARRL rules. All a ham need do to get an RCC is to present evidence of having had an actual one-on-one QSO that lasted at least 30 minutes, on any legal amateur frequency.<sup>[http://www.hfradio.net/bulletin/sept0504.html]</sup>

In order to permit the Rag Chewers Club (español: «Club de Radioaficionados Charlatanes») award to be made available free of charge to all licensed radio amateurs, certificates are distributed by e-mail as Adobe PDF files that can be printed and framed. The awards are numbered and endorseable by mode.  Complete rules and an on-line application are on the SPAR website at [http://www.spar-hams.org/index.php?pg=11 spar-hams.org].

== See also ==
* [[Awards and Certificates]]

== External links ==
* [http://www.qsl.net/ae0q/awards/wn0vpk_rcc_1968.htm WN0VPK 1968 Rag Chewers Club Certificate] awarded for having a QSO lasting 30 minutes or longer
* [http://www.cnunix.com/ftp/hamradio/oak.oakland/hamradio/arrl/bbs/general/awards.txt Historical list of ARRL awards]
* [http://www.arrl.org/awards/ Current list of ARRL awards]
* [http://www.spar-hams.org spar-hams.org Society for the Preservation of Amateur Radio]

<!--{{awards}}-->
{{operation}}
