In radio-amateur use, the term "simplex" is used to refer to bi-directional communication on a single frequency - distinguishing this mode of operation from the use of [[repeater]] stations which receive on one frequency while simultaneously re-transmitting messages on another.

Each of the radio-amateur bands will typically have one or more specific, common calling frequencies designated within the band segment allocated to simplex operation; [[2 metres]], for instance, uses 146.52MHz FM voice or 144.1MHz [[CW]] as the most common default simplex calling frequencies. See [[bands|individual bandplans]] for a more complete list.

''(Note that this definition is incompatible with the term "simplex" as defined in data processing, where the "simplex" terminology designates a connection which cannot be reversed in direction, such as an input-only link from a computer keyboard).''

{{operation}}
