Related wiki pages : [[Education]], [[Electronic Theory]]
----

<font size="+3">T</font>his written guide will help beginners and novices to obtain effective results when soldering '''electronic components'''. If you have little or no experience of using a soldering iron, then EPE ''(Everyday Practical Electronics ''magazine) recommends that you practice your soldering technique on some fresh surplus components and clean stripboard (protoboard), before experimenting with a proper constructional project. This will help you to avoid the risk of disappointment when you start to assemble your first prototypes. If you've never soldered before, then read on!

''Everyday Practical Electronics ''magazine contains the widest variety of interesting projects and information for beginners, trainees, hobbyists and professionals in electronics. This ''Basic Soldering Guide'' was condensed from our fully-illustrated series ''Build Your Own Projects'', written by Alan Winstanley and published in Everyday Practical Electronics magazine, from November 1996 to March 1997. EPE is read in more than sixty countries: to go to our Subscriptions Page, please [subrates.html click here]. For EPE Back Issue information, please <span class="normal">[shopdoor.html click here].</span>

Please refer to the [#copy Copyright Notice] appearing at the end.

----

==Soldering irons==

====Topics in this section include:====


<font size="+3">T</font>he most fundamental skill needed to assemble any electronic project is that of '''''soldering'''''. It takes some practice to make the perfect joint, but, like riding a bicycle, once learned is never forgotten! The idea is simple: to join electrical parts together to form an electrical connection, using a molten mixture of lead and tin (solder*) with a soldering iron. A large range of soldering irons is available - which one is suitable for you depends on your budget and how serious your interest in electronics is.

[*Note: the use of lead in solder is now increasingly prohibited in many countries. "Lead free" solder is now statutory instead.]

Electronics catalogues often include a selection of well-known brands of soldering iron. Excellent British-made ones include the universally popular Antex, Adcola and Litesold makes. Other popular brands include those made by Weller and Ungar. A very basic mains electric soldering iron can cost from under �5 (US$ 8), but expect a reasonable model to be approximately �10-�12 (US$ 16 - 20) - though it's possible to spend into three figures on a soldering iron "station" if you're really serious! Check some suppliers' catalogues for some typical types. Certain factors you need to bear in mind include:-

'''''<font color="#FF0000">Voltage:</font>''''' most irons run from the mains at 240V. However, low voltage types (e.g. 12V or 24V) generally form part of a "soldering station" and are designed to be used with a special controller made by the same manufacturer.

'''''<font color="#FF0000">Wattage:</font>''''' Typically, they may have a power rating of between 15-25 watts or so, which is fine for most work. A higher wattage does '''''not''''' mean that the iron runs hotter - it simply means that there is more power in reserve for coping with larger joints. This also depends partly on the design of the "bit" (the tip of the iron). Consider a higher wattage iron simply as being more "unstoppable" when it comes to heavier-duty work, because it won't cool down so quickly.

'''''<font color="#FF0000">Temperature Control:</font>''''''''' the simplest and cheapest types don't have any form of temperature regulation. Simply plug them in and switch them on! Thermal regulation is "designed in" (by physics, not electronics!): they may be described as "thermally balanced" so that they have some degree of temperature "matching" but their output will otherwise not be controlled. Unregulated irons form an ideal general purpose iron for most users, and they generally cope well with printed circuit board soldering and general interwiring. Most of these "miniature" types of iron will be of little use when attempting to solder large joints (e.g. very large terminals or very thick wires) because the component being soldered will "sink" heat away from the tip of the iron, cooling it down too much. (This is where a higher wattage comes in useful.)

A proper temperature-controlled iron will be quite a lot more expensive - retailing at say �40 (US$ 60) or more - and will have some form of built-in thermostatic control, to ensure that the temperature of the bit (the tip of the iron) is maintained at a fixed level (within limits). This is desirable especially during more frequent use, since it helps to ensure that the temperature does not "overshoot" in between times, and also guarantees that the output will be relatively stable. Some irons have a bimetallic strip thermostat built into the handle which gives an audible "click" in use: other types use all-electronic controllers, and some may be adjustable using a screwdriver.

Yet more expensive still, <font color="#FF0000">'''''soldering stations'''''</font> cost from �70 (US$ 115) upwards (the iron may be sold separately, so you can pick the type you prefer), and consist of a complete bench-top control unit into which a special'' low-voltage'' soldering iron is plugged. Some versions might have a built-in digital temperature readout, and will have a control knob to enable you to vary the setting. The temperature could be boosted for soldering larger joints, for example, or for using higher melting-point solders (e.g. silver solder). These are designed for the most discerning users, or for continuous production line/ professional use. The best stations have irons which are well balanced, with comfort-grip handles which remain cool all day. A thermocouple will be built into the tip or shaft, which monitors temperature.

'''''<font color="#FF0000">Anti-static protection:</font>''''' if you're interested in soldering a lot of static-sensitive parts (e.g. CMOS chips or MOSFET transistors), more advanced and expensive soldering iron stations use static-dissipative materials in their construction to ensure that static does not build up on the iron itself. You may see these listed as "ESD safe" ''(electrostatic discharge proof).'' The cheapest irons won't necessarily be ESD-safe but never the less will still probably perform perfectly well in most hobby or educational applications, if you take the usual anti-static precautions when handling the components. The tip would need to be well earthed (grounded) in these circumstances.

'''''<font color="#FF0000">Bits:</font>''''' it's useful to have a small selection of manufacturer's bits (soldering iron tips) available with different diameters or shapes, which can be changed depending on the type of work in hand. You'll probably find that you become accustomed to, and work best with, a particular shape of tip. Often, tips are'' iron-coated'' to preserve their life, or they may be bright-plated instead. Copper tips are seldom seen these days.

'''''<font color="#FF0000">Spare parts:</font>''''' it's nice to know that spare parts may be available, so if the element blows, you don't need to replace the entire iron. This is especially so with expensive irons. Check through some of the larger mail-order catalogues.

You will occasionally see '''''<font color="#FF0000">gas-powered</font>''''' soldering irons which use butane rather than the mains electrical supply to operate. They have a catalytic element which, once warmed up, continues to glow hot when gas passes over them. Service engineers use them for working on repairs where there may be no power available, or where a joint is tricky to reach with a normal iron, so they are really for occasional "on the spot" use for quick repairs, rather than for mainstream construction or assembly work. One example is the Maplin PG509, given a full review with photographs [gas-soldering-iron.htm here].

Another technique is the proprietary "Coldheat" battery powered soldering iron that we reviewed [cold-soldering.htm here]. There are a number of reasons why this should only be used with extreme care (if at all) on electronic circuitboards.

A '''''solder gun''''' is a pistol-shaped iron, typically running at 100W or more, and is completely unsuitable for soldering modern electronic components: they're too hot, heavy and unwieldy for micro-electronics use. Plumbing, maybe..!

Soldering irons are best used along with a heat-resistant '''''bench-type holder''''', so that the hot iron can be safely parked in between use. Soldering stations already have this feature, otherwise a separate soldering iron stand is essential, preferably one with a holder for tip-cleaning sponges. Now let's look at how to use soldering irons properly, and how to put things right when a joint goes wrong.


==How to solder==


<font size="+3"> T</font>urning to the actual techniques of soldering, firstly it's best to ''secure the work'' somehow so that it doesn't move during soldering and affect your accuracy. In the case of a printed circuit board, various holding frames are fairly popular especially with densely populated boards: the idea is to insert all the parts on one side ("stuffing the board"), hold them in place with a special foam pad to prevent them falling out, turn the board over and then snip off the wires with cutters before making the joints. The frame saves an awful lot of turning the board over and over, especially with large boards. Other parts could be held firm in a modeller's small vice, for example.

Solder joints may need to possess some degree of mechanical strength in some cases, especially with wires soldered to, say, potentiometer or switch tags, and this means that the wire should be looped through the tag and secured before solder is applied. The down side is that it is more difficult to'' de-solder'' the joint (see later) and remove the wire afterwards, if required. Otherwise, in the case of an ordinary circuit board, components' wires are bent to fit through the board, inserted flush against the board's surface, splayed outwards a little so that the part grips the board, and then soldered.

In my view - opinions vary - it's generally better to snip the surplus wires leads off ''first'', to make the joint more accessible and avoid applying a mechanical shock to the p.c.b. joint. However, in the case of semiconductors, I often tend to leave the snipping until ''after'' the joint has been made, since the excess wire will help to sink away some of the heat from the semiconductor junction. Integrated circuits can either be soldered directly into place if you are confident enough, or better, use a dual-in-line socket to prevent heat damage. The chip can then be swapped out if needed.

Parts which become hot in operation (e.g. some resistors), are raised above the board slightly to allow air to circulate. Some components, especially large electrolytic capacitors, may require a mounting clip to be screwed down to the board first, otherwise the part may eventually break off due to vibration.

The perfectly soldered joint will be nice and shiny looking, and will prove reliable in service. I would say that:

* '''cleanliness'''
* '''temperature'''
* '''time'''
* '''adequate solder coverage'''

are the key factors affecting the quality of the joint. A little effort spent now in soldering the perfect joint may save you - or somebody else - a considerable amount of time in troubleshooting a defective joint in the future. The basic principles are as follows.

====Really Clean====

Firstly, and without exception, all parts - including the iron tip itself - must be '''clean''' and''' free from''' '''contamination'''. Solder just will not "take" to dirty parts! Old components or copper board can be notoriously difficult to solder, because of the layer of oxidation which builds up on the surface of the leads. This repels the molten solder and this will soon be evident because the solder will "bead" into globules, going everywhere except where you need it. ''Dirt is the enemy of a good quality soldered joint!''

Hence, it is an absolute necessity to ensure that parts are free from grease, oxidation and other contamination. In the case of old resistors or capacitors, for example, where the leads have started to oxidise, use a small hand-held file or perhaps scrape a knife blade or rub a fine emery cloth over them to reveal fresh metal underneath. Stripboard and copper printed circuit board will generally oxidise after a few months, especially if it has been fingerprinted, and the copper strips can be cleaned using an abrasive rubber block, like an aggressive eraser, to reveal fresh shiny copper underneath.

Also available is a fibre-glass filament brush, which is used propelling-pencil-like to remove any surface contamination. These tend to produce tiny particles which are highly irritating to skin, so avoid accidental contact with any debris. Afterwards, a wipe with a rag soaked in cleaning solvent will remove most grease marks and fingerprints. After preparing the surfaces, avoid touching the parts afterwards if at all possible.

Another side effect of having dirty surfaces is the tendency for people to want to apply'' more heat'' in an attempt to "force the solder to take". This will often do more harm than good because it may not be possible to burn off any contaminants anyway, and the component may be overheated. In the case of semiconductors, temperature is quite critical and they may be harmed by applying such excessive heat.

Before using the iron to make a joint, it should be "tinned" (coated with solder) by applying a few millimetres of solder, then wiped on a damp sponge preparing it for use: you should always do this immediately with a new bit, anyway. Personally, I always re-apply a very small amount of solder again, mainly to improve the thermal contact between the iron and the joint, so that the solder will flow more quickly and easily. It's sometimes better to tin larger parts as well before making the joint itself, but it isn't generally necessary with p.c.b. work. (All'' EPE'' printed circuit boards are "roller-tinned" to preserve their quality and to help with soldering.) A worthwhile product is Weller's ''Tip Tinner & Cleaner'', a small 15 gram tinlet of paste onto which you dab a hot iron - the product cleans and tins the iron ready for use. An equivalent is Adcola ''Tip-Save.''

Normal electronics grade solder is now "lead free" and typically contains Sn 97 Ag 2.5 Cu 0.5 (i.e. 97% tin, 2.5% silver and 0.5% copper). '''I''''''t already contains cores of "flux"''' which helps the molten solder to flow more easily over the joint. Flux removes oxides which arise during heating, and is seen as a brown fluid bubbling away on the joint. <span class="style2">The use of separate acid flux paste (e.g. as used by plumbers) should NEVER be necessary in normal electronics applications because electronics-grade solder already contains the correct grade of flux!</span> Other solders are available for specialist work, including aluminium and silver-solder. Different solder diameters are produced, too; 20-22 SWG (19-21 AWG) is 0.91-0.71mm diameter and is fine for most work. Choose 18 SWG (16 AWG) for larger joints requiring more solder.

====Temperature====

Another step to successful soldering is to ensure that the '''temperature '''of ''all'' the parts is raised to roughly the same level before applying solder. Imagine, for instance, trying to solder a resistor into place on a printed circuit board: it's far better to heat ''both'' the copper p.c.b. ''and'' the resistor lead at the same time before applying solder, so that the solder will flow much more readily over the joint. Heating one part but not the other is far less satisfactory joint, so strive to ensure that the iron is in contact with ''all'' the components first, before touching the solder to it. The melting point of most solder is in the region of 188�C (370�F) and the iron tip temperature is typically 330-350�C (626�-662�F). The latest lead-free solders typically require a higher temperature.

====Now is the time====

Next, the joint should be heated with the bit for just the right amount of '''time''' - during which a short length of solder is applied to the joint. Do '''not''' use the iron to carry molten solder over to the joint! Excessive time will damage the component and perhaps the circuit board copper foil too! Heat the joint with the tip of the iron, then continue heating whilst applying solder, then remove the iron and allow the joint to cool. This should take only a few seconds, with experience. The heating period depends on the temperature of your iron and size of the joint - and larger parts need more heat than smaller ones - but some parts (semiconductor diodes, transistors and i.c.s), are sensitive to heat and should not be heated for more than a few seconds. Novices sometimes buy a small clip-on heat-shunt, which resembles a pair of aluminium tweezers. In the case of, say, a transistor, the shunt is attached to one of the leads near to the transistor's body. Any excess heat then diverts up the heat shunt instead of into the transistor junction, thereby saving the device from over-heating. Beginners find them reassuring until they've gained more experience.

====Solder Coverage====

The final key to a successful solder joint is to apply an appropriate amount of solder. ''Too much solder'' is an unnecessary waste and may cause short circuits with adjacent joints. ''Too little'' and it may not support the component properly, or may not fully form a working joint. How much to apply, only really comes with practice. A few millimetres only, is enough for an "average" p.c.b. joint, (if there is such a thing).

----

==Desoldering methods==

<font size="+3">A</font> soldered joint which is improperly made will be electrically "noisy", unreliable and is likely to get worse in time. It may even not have made any electrical connection at all, or could work initially and then cause the equipment to fail at a later date! It can be hard to judge the quality of a solder joint purely by appearances, because you cannot say how the joint actually formed on the ''inside'', but by following the guidelines there is no reason why you should not obtain perfect results.

A joint which is poorly formed is often called a "dry joint". Usually it results from dirt or grease preventing the solder from melting onto the parts properly, and is often noticeable because of the tendency of the solder not to "spread" but to form beads or globules instead, perhaps partially. Alternatively, if it seems to take an inordinately long time for the solder to spread, this is another sign of possible dirt and that the joint may potentially be a dry one.

There will undoubtedly come a time when you need to ''remove'' the solder from a joint: possibly to replace a faulty component or fix a dry joint. The usual way is to use a '''''desoldering pump''''''' ''or vacuum pump which works like a small spring-loaded bicycle pump, only in reverse! (More demanding users using CMOS devices might need a pump which is ESD safe.) A spring-loaded plunger is released at the push of a button and the molten solder is then drawn up into the pump. It may take one or two attempts to clean up a joint this way, but a small desoldering pump is an invaluable tool especially for p.c.b. work.

Sometimes, it's effective to actually ''add more'' solder and then desolder the whole lot with a pump, if the solder is particularly awkward to remove. Care is needed, though, to ensure that the boards and parts are not damaged by excessive heat; the pumps themselves have a P.T.F.E. nozzle which is heat proof but may need replacing occasionally.

An excellent alternative to a pump is to use ''desoldering braid'', including the famous American "Soder-Wick" (sic) or Adcola "TISA-Wick" which are packaged in small dispenser reels. This product is a specially treated fine copper braid which draws molten solder up into the braid where it solidifies. The best way is to use the tip of the hot iron to press a short length of braid down onto the joint to be de-soldered. The iron will subsequently melt the solder, which will be drawn up into the braid. Take extreme care to ensure that you don't allow the solder to cool with the braid adhering to the work, or you run the risk of damaging p.c.b. copper tracks when you attempt to pull the braid off the joint. See my photo gallery for more details.

I recommend buying a small reel of de-soldering braid, especially for larger or difficult joints which would take several attempts with a pump. It is surprisingly effective, especially on difficult joints where a desoldering pump may prove a struggle.

----

===Here's a summary of how to make the perfect solder joint.===

# All parts must be clean and free from dirt and grease.
# Try to secure the work firmly.
# "Tin" the iron tip with a small amount of solder. Do this immediately, with new tips being used for the first time.
# Clean the tip of the hot soldering iron on a damp sponge.
# Many people then add a tiny amount of fresh solder to the cleansed tip.
# Heat all parts of the joint with the iron for under a second or so.
# Continue heating, then apply sufficient solder only, to form an adequate joint.
# Remove and return the iron safely to its stand.
# It only takes two or three seconds at most, to solder the average p.c.b. joint.
# Do not move parts until the solder has cooled.

===Troubleshooting Guide===

* Solder won't "take" - grease or dirt present - desolder and clean up the parts. Or, material may not be suitable for soldering with lead/tin solder (eg aluminium).
* Joint is crystalline or grainy-looking - has been moved before being allowed to cool, or joint was not heated adequately - too small an iron/ too large a joint.
* Solder joint forms a "spike" - probably overheated, burning away the flux.

===First Aid===

If you are unlucky enough to receive burns which require treatment, here's what to do :-

# Immediately cool the affected area with cold running water for several minutes.
# Remove any rings etc. before swelling starts.
# Apply a sterile dressing to protect against infection.
# Do not apply lotions, ointments etc., nor prick any blisters which form later.
# Seek professional medical advice where necessary.


===How to contact the author===

'''Written by Alan Winstanley''' Email to [mailto:alan@epemag.demon.co.uk alan@epemag.demon.co.uk].

----

===Copyright Notice===

<font size="2" face="Arial, Helvetica, sans-serif">Text � 1996-2006 Wimborne Publishing Limited, Wimborne, Dorset, England. Everyday Practical Electronics Magazine has provided this document as a free web resource to help constructors, trainees and students. You are welcome to download it, print it and distribute it for personal or educational use. It may not be used in any commercial publication, mirrored on any commercial site nor may it be appended to or amended, or used or distributed for any commercial reason, without the prior permission of the Publishers.</font>

<font size="2" face="Arial, Helvetica, sans-serif">Photographs � 1996-2006 Alan Winstanley WORLD COPYRIGHT RESERVED</font>

<font size="2" face="Arial, Helvetica, sans-serif">Every care has been taken to ensure that the information and guidance given is accurate and reliable, but since conditions of use are beyond our control no legal liability or consequential claims will be accepted for any errors herein.</font>

<font size="2" face="Arial, Helvetica, sans-serif">The British mains voltage supply is 230V a.c. and you should amend ratings for local conditions.</font>

<font size="2" face="Arial, Helvetica, sans-serif">Please check the [http://www.epemag.wimborne.co.uk Everyday Practical Electronics Web Site] for details of the current issue, subscription rates, Back issue availability and contact information.</font>

''''
{{electronics}}
