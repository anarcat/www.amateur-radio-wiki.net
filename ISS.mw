Related Wiki pages: [[satellites]], [[AO51]], [[VO52]], [[SO50]], [[FO29]]

The '''International Space Station''' is populated by astronauts who are all Amateurs. ISS partner countries USA, Canada, Russia, Europe and Japan, operate '''ARISS''' - Amateur Radio on the International Space Station.

To downlink is 145.800 FM. (There other frequencies outside the Amateur ranges such as 143.625 FM on which you can hear ISS to base communications)

== Working the astronauts ==

In Region 1 (Europe-Middle East-Africa-North Asia), the uplink is 145.200 FM.<br>In Region 2, (North and South America-Caribbean-Greenland-Australia-South Asia) it's 144.490 FM

Please be aware that there are scheduled links with schools, and please don't attempt a QSO if someone aboard the ISS appears to be answering questions.

== Working the cross-band repeater ==

To use the cross-band repeater, set your uplink to 437.800 FM (worldwide). However, according to "ISSFanClub" as of 2008-10-19, it hasn't been used for 255 days.

== Images ==

Occasionally, the 145.800 FM downlink will squirt some data ever 5 mins. This appears to be images.

Images can be [http://www.amsat.com/ARISS_SSTV/submit.php submitted] to ARISS, and are visible here: http://www.amsat.com/ARISS_SSTV/

== Tracking the ISS ==

The ISS will take about 12 minutes to pass from horizon to horizon if it goes directly overhead. The signals (as received on a mobile antenna (6/2/70cms), leant backwards to "go horizontal") are 5/9+30 at the peak of the pass. It's a very strong signal as the station is only 200-300 miles away (straight up!).

However, from that height, the ISS can "see" a large area of earth (footprint) - all with many stations wanting a contact.

== ARISS for schools ==

ARISS school contacts may be scheduled where amateur satellite operators deploy a portable station at the school so that, during a ten minute pass of the ISS, an astronaut may answer questions prepared by the students. If a station cannot be deployed portably at the school for a direct link, a remote ground station may be linked by landline to provide a telebridge.

Schools can apply to have a scheduled contact here: http://www.ariss.org

While the ISS operators are unavailable for other QSO's during times scheduled for school contacts, [[SWL]]/scanner reception reports referencing communications between ISS and individual schools will be accepted.

== External links ==
* http://www.ariss.org
* [http://www.ariss-eu.org/ ARISS Europe]
* [http://www.rac.ca/ariss/oindex.htm ARISS Canada]
* [http://www.arrl.org/ARISS/ ARISS: Amateur Radio on the International Space Station], ARRL, December 2006
* A very good page for more information is http://www.issfanclub.com/


{{satellites}}
