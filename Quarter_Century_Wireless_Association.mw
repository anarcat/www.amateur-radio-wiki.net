The '''Quarter Century Wireless Association''' is an organisation for radio amateurs who can show they were first licensed at least twenty-five years ago and are still licensed as radio amateurs today. Founded on December 5 1947, it operates as a national organisation and a series of local chapters. Of the 123 currently-active local groups, two are in Europe, four in Canada and the others are located in the United States. Members of the national organisation are allowed to also join a local chapter upon payment of chapter membership dues.

While [[CW]] ability is not directly indicated as a criterion for membership, QCWA's members would have been licensed in an era in which Morse code was required for licensees to access the most-used amateur radio [[bands]]. QCWA does not require that a member have been licensed continuously throughout the twenty-five year period.

== See also ==
* [[Clubs/Canada]]
* [[Clubs/USA]]

== External links ==
* [http://www.qcwa.org Quarter Century Wireless Association]
* [http://www.qcwa.org/chapters-by-qth.htm List of chapters]
* [http://www.qsl.net/qcwa120 Quarter Century Wireless Women]

Canadian chapters:
* [http://www.obsd.com/qcwa-chapter151 Alberta]
* [http://qcwa70.org Ottawa]
* [http://www.qcwa.ca Southern Ontario]

US chapters:
* [http://www.qcwa154.com Coachella Valley, California]
* [http://www.home.earthlink.net/~aaparker/QCWA_MONTEREY_BAY_CHAPTER_191.htm Monterey Bay, California]
* [http://www.qsl.net/qcwa-11 Redwood City, California]
* [http://qcwa-58.rmhcn.org Colorado]
* [http://www.qcwa149.org Connecticut]
* [http://www.crompton.com/QCWA/index.html Delaware Valley/NJ]
* [http://www.qcwa150.com Delaware/Mar/VA]
* [http://www.edison196.org Fort Myers, Florida]
* [http://www.qrn.com/qcwa Gainesville, Florida]
* [http://sites.google.com/site/qcwa Orlando, Florida]
* [http://www.qcwa.org/chapter195.htm Sun City, Florida]
* [http://www.chem.hawaii.edu/uham/qcwa.html Hawaii]
* [http://www.qcwa49.org Atlanta, Georgia]
* [http://chapter36.org/ Northwest Indiana]
* [http://home.fuse.net/k8dv/qcwa/qcwa214.html Kentucky]
* [http://www.geocities.com/CapeCanaveral/Lab/4373 Brunswick, Maine]
* [http://www.qsl.net/qcwa35 Kansas City, Missouri/Kansas]
* [http://qcwa19.com/default.aspx St Louis, Missouri]
* [http://www.qcwa76.org Hendersonville, North Carolina]
* [http://www.monmouth.com/~eroswell/qcwa138.htm Central New Jersey]
* [http://www.feustel.us/HamRadio/QCWA Claremont, New Hampshire]
* [http://www.qcwa.com Long Island, New York]
* [http://www.lockport-ny.com/QCWA Lockport/Niagara, New York]
* [http://members.cox.net/renewsome Omaha, Nebraska]
* [http://www.qcwachapter190.org Reno, Nevada]
* [http://www.qsl.net/qcwa126 Piedmont, North Carolina]
* [http://www.cmh.net/qcwa Canton, Ohio]
* [http://www.qcwa-cleveland-1.org Cleveland, Ohio]
* [http://www.qcwa.org/qcwa212/index.htm Mid-Ohio]
* [http://www.qcwa.org/qcwa063/index.html Central Oklahoma]
* [http://www.geocities.com/qcwa220 Southern Oregon]
* [http://qcwa.honeysoftware.net Willamette Valley, Oregon]
* [http://www.enter.net/~w3zmn/qcwa17/qcwa17.html Allentown, Pennsylvania]
* [http://www.nittany.ws/qcwa Central Pennsylvania]
* [http://users.rcn.com/k1igd Rhode Island/MA]
* [http://www.qsl.net/w4qw Northeast Tennessee]
* [http://www.qcwa.us Dallas, Texas]
* [http://www.qsl.net/wa5cmi/qcwa Garland, Texas]
* [http://pob.swva.net/qcwa202 Virginia]
* [http://web.me.com/rrucker/chapter91 Merrifield, Virginia/Washington DC]
* [http://www.qcwa.org/chapter162.htm Racine, Wisconsin]

{{organisations}}
