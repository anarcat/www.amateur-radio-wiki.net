Dipoles are one of the first antennas ever created. Indeed, [[Hertz]] himself is often credited as the inventor of the dipole, in 1886). It's also the simplest antenna designs out there, while staying very powerful and useful, in all bands.

Dipole antennas are "two-sided' in that they are fed from a point between the two ends. In its simplest form, a dipole will be centre-fed, with the total length being a half-wavelength at the frequency of transmission.

''Main article: [[Wikipedia:Dipole_antenna]]''

== Centre-fed half-wave dipole ==

Half-wave centre-fed dipoles with each leg 1/4 wavelength long are perhaps the most common antennas used in amateur radio.  They are an example of a [[balanced antenna]] which does not require a [[centerpoise]] or other [[RF ground]].

[[Image:Vk4yeh_Dipole_antenna.png]]

However, in the above diagram, the coaxial cable is not a balanced feeder because the outer shield is grounded and therefore the feedline itself can radiate. This can be solved by using a [[balun]].

These 1/2 wave dipoles can be used on their odd [[harmonics]] as well as their [[fundamental frequency|fundamental resonant frequencies]].  For example, the amateur [[40 metres|40 metre band]] begins at 7 MHz.  The 3rd harmonic is 3 x 7 = 21 MHz, where the [[15 metres|15 metre band]] begins.  Thus a centre-fed dipole that is a half-wavelength long at [[40 metres]] (approximately 20 metres long, or 66 feet) can also be used as a [[15 metres|15 metre]] antenna.

Dipole length for resonance is calculated using either:

<math>L = \frac {468}{frequency(MHz)}</math> for total length in feet, '''or''',

<math>L = \frac {142.5}{frequency(MHz)}</math> for total length in metres.

== Off-centre-fed dipole (OCF dipole) ==

As the name suggests, the OCF dipole is not fed from the centre.  The advantage of a 1/2 wave OCF dipole fed 1/3 of the way from one end is that it can be used on the [[fundamental frequency]] and its even [[harmonics]].  Since most amateur bands are even harmonics of other amateur bands, this is a useful features.  An OCF that is 1/2 wavelength long on 80 metres (40 metres long, or 132 feet) can be used on [[80 metres|80]], [[40 metres|40]], [[20 metres|20]], or [[10 metres]].

L is calculated from the formulae above. The dimensions D1, D2 and D3 are found using:

<math>D1 = 0.38L</math>  

<math>D2 = 0.62L</math>  

<math>D3 = 0.14L</math>

[[Image:Vk4yeh_ocf_dipole.jpg|550px]]

== G5RV antenna ==

Although the G5RV is not a 1/2 wave dipole like those described above, this antenna (invented by Louis Varney, callsign G5RV) shares some physical characteristics with a centre-fed dipole. The main difference between a dipole and G5RV is that the transmission line of a G5RV is part of the [[impedance matching|matching]] section that allows operation over a number of bands. Although the G5RV can be used on [[20 metres]] without an [[antenna tuner]], a tuner is necessary to use it on other bands.

[[Image:G5RV.jpg | 700px]]

There are several different variations on the G5RV.  The most well-known is fed through 450-ohm [[ladder line]] 10.6 metres (34.8 feet) long, with one end of the ladder line connected at the centre-feed point of the antenna and the other connected through a [[balun]] to 50-ohm [[Coaxial Cable]] [[feedline]].  An alternative is to run [[balanced feedline]] (such as [[twin-lead]] or [[ladder line]]) all the way from the antenna [[feedpoint]] to the [[antenna tuner]].  The latter arrangement can be very effective, but not all lengths of [[feedline]] will tune properly.  (See detailed discussion on [[feedline|feedlines]].)  Standard G5RV antennas are 31 metres (102 feet) long (excluding feedline and matching section length), and can be used on [[80 metres|80]], [[40 metres|40]], [[20 metres|20]], [[15 metres|15]], and [[10 metres]].  A half-length G5RV 51 feet long can be used on [[40 metres|40]], [[20 metres|20]], [[15 metres|15]], and [[10 metres|10]]; this variation is known as the '''G5RV jr'''.

The antenna can be installed as a horizontal dipole or an inverted V. If configured as such, the included angle at the apex should exceed 120 degrees. This means, for example, that a junior G5RV will be elevated at most 13 feet higher than the ends of the dipole. To calculate this, you can use the following, where we split the triangle created by the inverted V in two square triangles to apply simple [[Wikipedia:Trigonometric_functions|trigonometry]]:

<math>\frac{height}{hypotenuse} = \cos(half of included angle)</math>

hence:

<math>height = hypothenuse * \cos(half of included angle) = \frac{51}{2} * \cos(\frac{120}{2}) = 26.5 * \cos(60) = 13.25</math>

=== Reviews and references ===

* Excellent analyses of the G5RV have been written by [http://www.cebik.com/wire/g5rv.html W4RNL] and [http://www.vk1od.net/G5RV/ VK1OD]
* Philip Grimshaw (VK4KVK) has written a comprehensive [http://www.grimshaw.net.au/vkg5rvhb.html book] on the G5RV
* [[Construct a G5RV]]
* [[VK2ACY - G5RV coupler]] - Wayne (VK2ACY) has produced a coupler for the G5RV that operates on 80m, 40m and 20m
* [http://www.jackclarke.net/g5rvhalf.htm  G5RV using 300ohm ribbon feeder] Construction details from Jack VE3EED

== Folded Dipole ==

'''Folded dipoles''' are also 1/2 wavelength long.  (The Greek letter <math>\lambda</math> is commonly used to represent wavelength.)  However, the radiating elements are made of two parallel wires that are connected at the ends, and the feedpoint is at the centre of one of the two wires. The advantage of the folded dipole is that its impedance is about 300 ohms, allowing it to be fed with a 300-ohm balanced line.  It is possible to make both the radiating elements and the [[feedline]] from 300-ohm [[twinlead]], although some type of [[antenna tuner]] or [[matching section]] will be required to transform the impedance to 50 ohms at the [[Transceivers|transceiver]].

These antennas are commonly used on FM [[repeater|repeaters]] on [[VHF]] bands, although care should be taken using 300-ohm [[twinlead]] at [[VHF]] and higher frequencies.  (See discussion on [[feedline]].)

[[Image:vk4yeh_folded_dipole.jpg|550px]]

Another advantage of a folded dipole is that it has a wide usable bandwidth, allowing it to cover more of the band.  However, folded dipoles cannot be used as part of a [[dipole#fan dipole|fan dipole]], and unlike conventional single-wire dipoles, they cannot be used on their even harmonics - they are resonant on odd harmonics.  (Both of these limitations occur because conventional dipoles have higher [[impedance]] away from their resonant frequencies and reach minimum [[impedance]] at [[resonance]], while folded dipoles have lower [[impedance]] away from [[resonance]] and approach maximum [[impedance]] at [[resonance]].  At DC, a folded dipole is a direct short across the feedpoint, while a conventional dipole is an open circuit.)

== Fan Dipole ==

Several dipoles with different lengths, each of which is resonant on a different band, can be connected together at a common [[feedpoint]] and fed with a single [[feedline]].  This is known as a fan dipole.  The radiating elements of the dipoles should not run parallel to each other.

[[Image:vk4yeh_fan_dipole.jpg|550px]]

Each of the dipole element pairs A, B and C in this diagram would usually be cut for a a frequency in the centre of the required amateur band. The total length of each dipole pair is 1/2<math>\lambda</math>.

'''Approximate total lengths of dipole elements'''

<table border=1>
<tr>
<td>Amateur band</td>
<td>total length (feet)</td>
<td>total length (metres)</td>
</tr>
<tr>
<td>80 metre band</td>
<td>120 feet</td>
<td>37 metres</td>
</tr>
<tr>
<td>40 metre band</td>
<td>66 feet</td>
<td>20 metres</td>
</tr>
<tr>
<td>20 metre band</td>
<td>33 feet</td>
<td>10.2 metres</td>
</tr>
<tr>
<td>10 metre band</td>
<td>16.5 feet</td>
<td>5.0 metres</td>
</tr>
</table>

== Coaxial Dipole (Double Bazooka)==


The coaxial dipole antenna consists of a half-wavelength section of coax line with the shield opened a the center and feed line attached to the open ends of the shield.  The outside conductor of the coax acts a half-wave dipole in combination with the open wire end sections of the antenna. The inside sections, do not radiate, but act as quarter-wave shorted stubs which presents high resistive impedance to the feed point at resonance and tends to cancel reactance as frequency off resonant frequencies, thus increasing band width. 

The antenna can be cut for any operating frequency, from 160-meters down. The RG-58U is capable of handling a full kilowatt. This design is broad-banded will provided low SWR over the entire 80 and 40 meter bands. Construction techniques are not critical. It can be put together with insulators and relief strains or thrown together in an emergency with just twisting and taping. How well its built will determine how long it stays up of course.

The coaxial dipole antenna is perfect for stealth work. As it is insulated it can be placed in trees, under eaves or next to house trim even in attics. It can be put up as a dipole, inverted V, vertical dipole and sloper. Its ends can be bent to accommodate unusual spaces. The 40 meter antenna can be used for 15 meters. 

[[Image:Bazooka.jpg |650px]]

'''Dimensions guide'''

[[Image: Bazooka(2).jpg |600px]]

==Coaxial Trap Dipole==

The Coaxial Trap Dipole is a symmetrical wire antenna whith traps at various points along the dipole arms to make them resonant at the required frequencies.

The diagram below shows approximate positions for coaxial traps. These should be verified by cutting them longer and trimming to the achieve resonance.

[[Image:Coaxial_trap_dipole.jpg  | 750px]]

===Coaxial traps===

Coaxial traps are coils of coaxial cable wound around cylindrical formers as shown in the diagram below. Please note the connection details inside the former.

[[Image:Coaxial_trap.jpg |520px]]

Formers can be strengthened in a number of ways to avoid stress on the solder joints:

* making a wire "handle" passing through each and of the former. Solder the internal wiring to the the handle where it passes through the end of the former, and the "dipole wire" to the other end of the handle.

* Passing a wood, plastic, fibreglass rod through the former. Use this to provide support for the solder joints.

* The traps can be waterproofed by filling them with silicon sealer.

===Trap dimensions and frequencies===

{| style="border-collapse: collapse; border: none; mso-border-alt: solid windowtext .5pt; mso-padding-alt: 0mm 5.4pt 0mm 5.4pt" border="1"
| style="width: 47.95pt; border: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>F</center>
| style="width: 75.6pt; border: solid windowtext .5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>20mm</center>
| style="width: 75.65pt; border: solid windowtext .5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>32mm</center>
| style="width: 75.6pt; border: solid windowtext .5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>65mm</center>
| style="width: 75.65pt; border: solid windowtext .5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>90mm</center>
| style="width: 75.65pt; border: solid windowtext .5pt; border-left: none; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>100mm</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>1.83</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 90.45</center>

<center>L = 44.77</center>

<center>C L= 711.51</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 51.15</center>

<center>L = 25.32</center>

<center>CL = 596.34</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 21.63</center>

<center>L = 10.71</center>

<center>CL = 477.85</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 15.07</center>

<center>L = 7.46</center>

<center>CL = 452.22</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 13.51</center>

<center>C = 6.69</center>

<center>CL = 448.03</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>3.65</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 45.81</center>

<center>L = 22.67</center>

<center>CL = 361.59</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 26.33</center>

<center>L = 12.03</center>

<center>CL = 308.17</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 11.80</center>

<center>L = 5.84</center>

<center>CL = 261.85</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 8.53</center>

<center>L = 4.22</center>

<center>CL = 257.00</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 7.73</center>

<center>L = 3.83</center>

<center>CL = 257.56</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>7.05</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 24.14</center>

<center>L = 11.95</center>

<center>CL = 191.73</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 14.22</center>

<center>L = 7.04</center>

<center>CL = 167.64</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 6.82</center>

<center>L = 3.88</center>

<center>CL = 152.44</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 5.09</center>

<center>L = 2.52</center>

<center>CL = 145.49</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 4.66</center>

<center>L = 2.31</center>

<center>CL = 156.24</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>14.17</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 12.41</center>

<center>L = 6.14</center>

<center>CL = 99.81</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 7.59</center>

<center>L = 3.76</center>

<center>CL = 90.69</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 3.93</center>

<center>L = 1.94</center>

<center>CL = 78.08</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 3.02</center>

<center>L = 1.49</center>

<center>CL = 92.53</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>18.115</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 9.87</center>

<center>L = 4.88</center>

<center>CL = 79.87</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 6.13</center>

<center>L = 3.04</center>

<center>CL = 73.73</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 3.25</center>

<center>L = 1.61</center>

<center>CL = 74.03</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>21.225</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 8.52</center>

<center>L = 4.22</center>

<center>CL = 69.34</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 5.35</center>

<center>L = 2.65</center>

<center>CL = 64.69</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>24.895</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 7.36</center>

<center>L = 3,65</center>

<center>Cl = 60.62</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 4.68</center>

<center>L = 2.31</center>

<center>Cl = 56.83</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
|-
| style="width: 47.95pt; border: solid windowtext .5pt; border-top: none; mso-border-top-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="48" valign="top" |
<center>28.8</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 6.45</center>

<center>L = 3.19</center>

<center>Cl = 53.11</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>T = 4.14</center>

<center>L = 2.05</center>

<center>CL = 50.59</center>
| style="width: 75.6pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
| style="width: 75.65pt; border-top: none; border-left: none; border-bottom: solid windowtext .5pt; border-right: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; padding: 0mm 5.4pt 0mm 5.4pt" width="76" valign="top" |
<center>�</center>
|}

== Other Dipole Variants ==
* '''"Rabbit ears"''' dipoles are commonly used for TV reception. The position and length of the dipole arms can be adjusted to optimise reception.
* A '''bowtie antenna''' is a dipole element in which each section of the driven element is narrow at the feedpoint and widens at the ends. As this yields a slightly wider bandwidth for the antenna, bowties are commonly used as driven elements on [[panel antenna]]s for wideband UHF broadcast reception.
* A loaded or '''shortened dipole''' may be constructed using inductive [[loading coil]]s in each half of the dipole. Like loading coils for vertical antennas, these may be installed at the feed point or (preferably) in the centre of each element section. As a dipole consists of two quarter-wave segments (instead of the single quarter-wave element of a vertical), two loading coils are required.
* A '''fan dipole''' (above) consists of multiple dipoles, each cut to a different length for a different band and spaced a few inches from each other. All of the individual dipole elements in the fan dipole share a common feedpoint. This design may be combined with the ''shortened dipole'' by providing loading coil pairs for each individual band. As these antennas can cover multiple bands (such as [[20m]], [[40m]], [[80m]]) using one relatively simple design, these are popular for portable radio-amateur operations such as [[Field Day]].
* Many of the most common complex antenna designs, such as [[yagi antenna]]s, [[log-periodic antenna|log-periodics]], [[panel antenna]]s or [[corner reflector]] antennas, are based on one or more dipoles as driven elements, supplemented by added reflectors or directors to add directionality and gain.

== See also ==

* [[Antennas]]
* [[Gain]]
* [[SWR]]
* [[Propagation]]

== External links ==
* [http://www.k7mem.com/Electronic_Notebook/antennas/shortant.html K7MEM: loaded-dipole antenna] web-based calculator
* [http://www.ehow.com/how_4528486_that-doesnt-require-antenna-tuner.html How to Build a Simple But Effective Dipole Antenna That Doesn't Require an Antenna Tune] - ehow.com
* [http://www.eham.net/articles/24060 Your First Dipole] - eham.net
* [http://www.k7mem.150m.com/Electronic_Notebook/antennas/dipole.html Center-Fed Half-Wave Dipole (3-30MHz) by Martin E. Meserve] - good examples on how to build a dipole and associated baluns
* [http://degood.org/coaxtrap/ An Attic Coaxial-Cable Trap Dipole for 10, 15, 20, 30, 40, and 80 Meters]



{{antennas}}
