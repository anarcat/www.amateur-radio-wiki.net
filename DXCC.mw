The '''DXCC''' is an award available to hams throughout the world, granted by the ARRL, to those who can verify two-way contacts to 100 different entities  from the [http://www.wia.org.au/members/awards/dxcccountrylist/ DXCC country list].  Verification can be either by [[QSL Bureaus|QSL]] cards or through logs submitted to [http://www.arrl.org/lotw/ Logbook of the World], also run by the ARRL.

'''DXCC''' is an acronym for '''[[DX]]''' '''C'''entury '''C'''lub

== Award types ==

=== Mode awards===

Any combination of [[Phone]], [[Modes#Continuous_Wave_.28CW.29|CW]], [[Modes#Radio_Teletype_.28RTTY.29|RTTY]], [[Satellites|Satellite]]

===Band awards===

* [[160 metres]]
* [[80 metres]]
* [[40 metres]]
* [[30 metres]]
* [[20 metres]]
* [[17 metres]]
* [[15 metres]]
* [[12 metres]]
* [[10 metres]]
* [[6 metres]]
* [[2 metres]]

===5 band DXCC===

Awarded to hams that have confirmed contacts 100 or more entities on each of the 80m, 40m, 20m, 15m and 10m amateur bands.  Further endorsements may be obtained for 100 confirmed contacts on the 160m, 0m, 17m, 12m, 6m and 2m amateur bands.

===QRP DXCC=== 

For amateurs who have confirmed 2 way contacts with 100 entities using  5 watts or less power, ([[QRP]])

== List of DXCC entities ==
Current DXCC entities (from the May 2009 ARRL DXCC list) are:

{|
!Entity ||Prefix 
|-
||Afghanistan ||YA 
|-
||Agalega & St. Brandon Is. ||3B6, 7 
|-
||Aland Is. ||OH0 
|-
||Alaska ||KL,AL,NL,WL 
|-
||Albania ||ZA 
|-
||Algeria ||7T-7Y 
|-
||American Samoa ||KH8 
|-
||Amsterdam & St. Paul Is. ||FT/Z 
|-
||Andaman & Nicobar Is. ||VU4 
|-
||Andorra ||C3 
|-
||Angola ||D2-3 
|-
||Anguilla ||VP2E
|-
||Annobon I. ||3C0 
|-
||Antarctica ||CE9/KC4
|-
||Antigua & Barbuda ||V2 
|-
||Argentina ||LO-LW 
|-
||Armenia ||EK 
|-
||Aruba ||P4
|-
||Ascension I. ||ZD8 
|-
||Asiatic Russia ||UA-UI8,9,0; RA-RZ 
|-
||Auckland & Campbell Is. ||ZL9 
|-
||Australia ||VK 
|-
||Austria ||OE 
|-
||Aves I. ||YV0 
|-
||Azerbaijan ||4J, 4K 
|-
||Azores ||CU 
|-
||Bahamas ||C6 
|-
||Bahrain ||A9 
|-
||Baker & Howland Is. ||KH1 
|-
||Balearic Is. ||EA6-EH6 
|-
||Banaba I. (Ocean I.) ||T33 
|-
||Bangladesh ||S2 
|-
||Barbados ||8P 
|-
||Belarus ||EU-EW 
|-
||Belgium ||ON-OT 
|-
||Belize ||V3 
|-
||Benin ||TY
|-
||Bermuda ||VP9 
|-
||Bhutan ||A5 
|-
||Bolivia ||CP 
|-
||Bonaire, Curacao ||PJ2, 4, 9 
|-
||Bosnia-Herzegovina ||E7
|-
||Botswana ||A2 
|-
||Bouvet ||3Y 
|-
||Brazil ||PP-PY 
|-
||British Virgin Is. ||VP2V
|-
||Brunei Darussalam ||V8 
|-
||Bulgaria ||LZ 
|-
||Burkina Faso ||XT
|-
||Burundi ||9U
|-
||C. Kiribati (British Phoenix Is.) ||T31 
|-
||Cambodia ||XU 
|-
||Cameroon ||TJ 
|-
||Canada ||VE, VO, VY 
|-
||Canary Is. ||EA8-EH8 
|-
||Cape Verde ||D4 
|-
||Cayman Is. ||ZF 
|-
||Central Africa ||TL
|-
||Ceuta & Melilla ||EA9-EH9 
|-
||Chad ||TT
|-
||Chagos Is. ||VQ9 
|-
||Chatham Is. ||ZL7 
|-
||Chesterfield Is. ||FK
|-
||Chile ||CA-CE 
|-
||China ||B 
|-
||Christmas I. ||VK9X 
|-
||Clipperton I. ||FO 
|-
||Cocos (Keeling) Is. ||VK9C 
|-
||Cocos I. ||TI9 
|-
||Colombia ||HJ-HK, 5J-5K 
|-
||Comoros ||D6
|-
||Congo (Republic of the) ||TN
|-
||Corsica ||TK 
|-
||Costa Rica ||TI, TE 
|-
||Cote d'Ivoire ||TU
|-
||Crete ||SV9, J49 
|-
||Croatia ||9A
|-
||Crozet I. ||FT/W 
|-
||Cuba ||CM, CO 
|-
||Cyprus ||5B, C4, P3 
|-
||Czech Republic ||OK-OL
|-
||DPR of Korea ||P5
|-
||Dem. Rep. of Congo ||9Q-9T 
|-
||Denmark ||OU-OW, OZ 
|-
||Desecheo I. ||KP5
|-
||Djibouti ||J2 
|-
||Dodecanese ||SV5, J45 
|-
||Dominica ||J7 
|-
||Dominican Republic ||HI 
|-
||Ducie I. ||VP6
|-
||E. Kiribati (Line Is.) ||T32 
|-
||East Malaysia ||9M6, 8
|-
||Easter I. ||CE0 
|-
||Ecuador ||HC-HD 
|-
||Egypt ||SU 
|-
||El Salvador ||YS, HU 
|-
||Equatorial Guinea ||3C 
|-
||Eritrea ||E3
|-
||Estonia ||ES 
|-
||Ethiopia ||ET 
|-
||European Russia ||UA-UI1,3,4,6; RA-RZ 
|-
||Falkland Is. ||VP8 
|-
||Faroe Is. ||OY 
|-
||Fed. Rep. of Germany ||DA-DR
|-
||Fernando de Noronha ||PP0-PY0F 
|-
||Fiji ||3D2 
|-
||Finland ||OF-OI 
|-
||France ||F 
|-
||Franz Josef Land ||R1FJ 
|-
||French Guiana ||FY 
|-
||French Polynesia ||FO 
|-
||Gabon ||TR
|-
||Galapagos Is. ||HC8-HD8 
|-
||Georgia ||4L 
|-
||Ghana ||9G
|-
||Gibraltar ||ZB2 
|-
||Glorioso Is. ||FR/G
|-
||Greece ||SV-SZ, J4 
|-
||Greenland ||OX 
|-
||Grenada ||J3 
|-
||Guadeloupe ||FG 
|-
||Guam ||KH2 
|-
||Guantanamo Bay ||KG4 
|-
||Guatemala ||TG, TD 
|-
||Guernsey ||GU, GP 
|-
||Guinea ||3X 
|-
||Guinea-Bissau ||J5 
|-
||Guyana ||8R 
|-
||Haiti ||HH 
|-
||Hawaii ||KH6,7 
|-
||Heard I. ||VK0 
|-
||Honduras ||HQ-HR 
|-
||Hong Kong ||VR 
|-
||Hungary ||HA, HG 
|-
||Iceland ||TF 
|-
||India ||VU 
|-
||Indonesia ||YB-YH
|-
||Iran ||EP-EQ 
|-
||Iraq ||YI 
|-
||Ireland ||EI-EJ 
|-
||Isle of Man ||GD, GT 
|-
||Israel ||4X, 4Z 
|-
||Italy ||I 
|-
||Jamaica ||6Y 
|-
||Jan Mayen ||JX 
|-
||Japan ||JA-JS, 7J-7N 
|-
||Jersey ||GJ, GH 
|-
||Johnston I. ||KH3 
|-
||Jordan ||JY 
|-
||Juan Fernandez Is. ||CE0 
|-
||Juan de Nova, Europa ||FR/J, E
|-
||Kaliningrad ||UA2 
|-
||Kazakhstan ||UN-UQ 
|-
||Kerguelen Is. ||FT/X 
|-
||Kermadec Is. ||ZL8 
|-
||Kingman Reef ||KH5K 
|-
||Kure I. ||KH7K 
|-
||Kuwait ||9K 
|-
||Kyrgyzstan ||EX 
|-
||Lakshadweep Is. ||VU7 
|-
||Laos ||XW 
|-
||Latvia ||YL 
|-
||Lebanon ||OD 
|-
||Lesotho ||7P 
|-
||Liberia ||EL 
|-
||Libya ||5A 
|-
||Liechtenstein ||HB0 
|-
||Lithuania ||LY 
|-
||Lord Howe I. ||VK9L 
|-
||Luxembourg ||LX 
|-
||Macao ||XX9 
|-
||Macedonia ||Z3
|-
||Macquarie I. ||VK0 
|-
||Madagascar ||5R 
|-
||Madeira Is. ||CT3 
|-
||Malawi ||7Q 
|-
||Maldives ||8Q 
|-
||Mali ||TZ
|-
||Malpelo I. ||HK0 
|-
||Malta ||9H 
|-
||Malyj Vysotskij I. ||R1MV 
|-
||Mariana Is. ||KH0 
|-
||Market Reef ||OJ0 
|-
||Marshall Is. ||V7 
|-
||Martinique ||FM 
|-
||Mauritania ||5T
|-
||Mauritius ||3B8 
|-
||Mayotte ||FH
|-
||Mellish Reef ||VK9M 
|-
||Mexico ||XA-XI 
|-
||Micronesia ||V6
|-
||Midway I. ||KH4 
|-
||Minami Torishima ||JD1
|-
||Moldova ||ER 
|-
||Monaco ||3A 
|-
||Mongolia ||JT-JV 
|-
||Montserrat ||VP2M
|-
||Morocco ||CN 
|-
||Mount Athos ||SV/A 
|-
||Mozambique ||C8-9 
|-
||Myanmar ||XY-XZ 
|-
||N. Cook Is. ||E5 
|-
||Namibia ||V5 
|-
||Nauru ||C2 
|-
||Navassa I. ||KP1 
|-
||Nepal ||9N 
|-
||Netherlands ||PA-PI 
|-
||New Caledonia ||FK 
|-
||New Zealand ||ZL-ZM 
|-
||Nicaragua ||YN,H6-7,HT 
|-
||Niger ||5U
|-
||Nigeria ||5N 
|-
||Niue ||ZK2 
|-
||Norfolk I. ||VK9N 
|-
||Northern Ireland ||GI, GN 
|-
||Norway ||LA-LN 
|-
||Ogasawara ||JD1
|-
||Oman ||A4 
|-
||Pakistan ||AP 
|-
||Palau ||T8,
|-
||Palmyra & Jarvis Is. ||KH5 
|-
||Panama ||HO-HP 
|-
||Papua New Guinea ||P2
|-
||Paraguay ||ZP 
|-
||Peru ||OA-OC 
|-
||Peter I I. ||3Y 
|-
||Philippines ||DU-DZ 
|-
||Pitcairn I. ||VP6 
|-
||Poland ||SN-SR 
|-
||Portugal ||CT 
|-
||Pratas I. ||BV9P 
|-
||Prince Edward & Marion Is. ||ZS8 
|-
||Puerto Rico ||KP3,4 
|-
||Qatar ||A7 
|-
||Reunion I. ||FR 
|-
||Revillagigedo ||XA4-XI4 
|-
||Rodriguez I. ||3B9 
|-
||Romania ||YO-YR 
|-
||Rotuma I. ||3D2 
|-
||Rwanda ||9X
|-
||S. Cook Is. ||E5 
|-
||Sable I. ||CY0 
|-
||Saint Barthelemy ||FJ
|-
||Saint Martin ||FS 
|-
||Samoa ||5W 
|-
||San Andres & Providencia ||HK0 
|-
||San Felix & San Ambrosio ||CE0 
|-
||San Marino ||T7 
|-
||Sao Tome & Principe ||S9 
|-
||Sardinia ||IS0, IM0 
|-
||Saudi Arabia ||HZ 
|-
||Scarborough Reef ||BS7 
|-
||Scotland ||GM, GS 
|-
||Serbia ||YT-YU 
|-
||Seychelles ||S7 
|-
||Sierra Leone ||9L 
|-
||Singapore ||9V
|-
||Slovak Republic ||OM
|-
||Slovenia ||S5
|-
||Solomon Is. ||H4 
|-
||Somalia ||T5, 6O 
|-
||South Africa ||ZR-ZU 
|-
||South Georgia I. ||VP8, LU 
|-
||South Orkney Is. ||VP8, LU 
|-
||South Sandwich Is. ||VP8, LU 
|-
||Sov. Mil. Order of Malta ||1A 
|-
||Spain ||EA-EH 
|-
||Spratly Is. || 
|-
||Sri Lanka ||4S 
|-
||St. Helena ||ZD7 
|-
||St. Kitts & Nevis ||V4
|-
||St. Lucia ||J6 
|-
||St. Maarten, Saba, St. ||PJ5-8 
|-
||St. Paul I. ||CY9 
|-
||St. Peter & St. Paul Rocks ||PP0-PY0S 
|-
||St. Pierre & Miquelon ||FP 
|-
||St. Vincent ||J8 
|-
||Sudan ||ST 
|-
||Suriname ||PZ 
|-
||Svalbard ||JW 
|-
||Swains I. ||KH8
|-
||Swaziland ||3DA 
|-
||Sweden ||SA-SM 
|-
||Switzerland ||HB 
|-
||Syria ||YK 
|-
||Taiwan ||BV 
|-
||Tajikistan ||EY 
|-
||Tanzania ||5H-5I 
|-
||Thailand ||HS, E2 
|-
||The Gambia ||C5 
|-
||Timor - Leste ||4W 
|-
||Togo ||5V 
|-
||Tokelau Is. ||ZK3 
|-
||Tonga ||A3 
|-
||Trindade & Martim Vaz Is. ||PP0-PY0T 
|-
||Trinidad & Tobago ||9Y-9Z 
|-
||Tristan da Cunha & Gough I. ||ZD9 
|-
||Tromelin I. ||FR/T 
|-
||Tunisia ||3V 
|-
||Turkey ||TA-TC 
|-
||Turkmenistan ||EZ 
|-
||Turks & Caicos Is. ||VP5 
|-
||Tuvalu ||T2
|-
||UK Sov. Base Areas on Cyprus ||ZC4
|-
||Uganda ||5X 
|-
||Ukraine ||UR-UZ, EM-EO 
|-
||United Arab Emirates ||A6 
|-
||United Nations HQ ||4U_UN 
|-
||Uruguay ||CV-CX 
|-
||Uzbekistan ||UJ-UM 
|-
||Vanuatu ||YJ 
|-
||Vatican ||HV 
|-
||Venezuela ||YV-YY, 4M 
|-
||Virgin Is. ||KP2 
|-
||W. Kiribati (Gilbert Is. ) ||T30 
|-
||Wake I. ||KH9 
|-
||Wales ||GW, GC 
|-
||Wallis & Futuna Is. ||FW 
|-
||West Malaysia ||9M2, 4
|-
||Western Sahara ||S0
|-
||Willis I. ||VK9W 
|-
||Yemen ||7O
|-
||Zambia ||9I-9J 
|-
||Zimbabwe ||Z2 
|}

== See also ==

* [[Contesting]]
* [[QSL Bureaus]]
* [[Bands]]
* [[Logging]]

== External links ==

* [[Wikipedia:DX Century Club]]
* [http://www.arrl.org/awards/dxcc/ DXCC] and [http://www.arrl.org/awards/dxcc/dxcclist.txt list of entities] (ARRL)

{{operation}}
