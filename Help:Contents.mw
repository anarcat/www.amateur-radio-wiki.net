'''PLEASE NOTE''' Editing of this page has been blocked because of repeated spam.  If you would like to edit this page please contact admin1@amateur-radio-wiki.net and it will be unlocked so that so that you can edit it.

== Page edits -frequently used markups ==

{| border = "1""
! style="width: 50%" | What it looks like
! style="width: 50%" | What you type
|-
|
You can ''italicize text'' by putting 2 
apostrophes on each side. 

3 apostrophes will embolden '''the text'''. 

5 apostrophes will embolden and italicize 
'''''the text'''''.

(4 apostrophes don't do anything special -- there's just ''''one left over''''.)
|<pre>
You can ''italicize text'' by putting 2 
apostrophes on each side. 

3 apostrophes will embolden '''the text'''. 

5 apostrophes will embolden and italicize 
'''''the text'''''.

(4 apostrophes don't do anything
special -- there's just ''''one left
over''''.)
</pre>
|-
|
You should "sign" your comments on talk pages:
* Three tildes give your user name: <br />
* Four tildes give your user name plus date/time: 07:46, 27 November 2005 (UTC)
* Five tildes give the date/time alone: 07:46, 27 November 2005 (UTC)
|<pre>
You should "sign" your comments 
on talk pages:
* Three tildes give your user
name: ~~~
* Four tildes give your user 
name plus date/time: ~~~~
* Five tildes give the 
date/time alone: ~~~~~
</pre>
|-
|
<div style="font-size:150%;border-bottom:1px solid rgb(170,170,170);">Section headings</div>

''Headings'' organize your writing into sections.
The Wiki software can automatically generate
a '''table of contents''' from them.

<div style="font-size:132%;font-weight:bold;">Subsection</div>
Using more equals signs creates a subsection.

<div style="font-size:116%;font-weight:bold;">A smaller subsection</div>

Don't skip levels, like from two to four equals signs.

Start with 2 equals signs not 1 because 1 creates H1 tags which should be reserved for page title.
|<pre>
== Section headings ==

''Headings'' organize your writing into sections.
The Wiki software can automatically generate
a table of contents from them.

=== Subsection ===

Using more equals signs creates a subsection.

==== A smaller subsection ====
Don't skip levels, 
like from two to four equals signs.

Start with 2 equals signs not 1 
because 1 creates H1 tags
which should be reserved for page title.
</pre>
|- id="lists"
|
* ''Unordered lists'' are easy to do:
** Start every line with a star.
*** More stars indicate a deeper level.
*: Previous item continues.
** A new line
* in a list  
marks the end of the list.
*Of course you can start again.
|<pre>
* ''Unordered lists'' are easy to do:
** Start every line with a star.
*** More stars indicate a deeper level.
*: Previous item continues.
** A new line
* in a list  
marks the end of the list.
* Of course you can start again.
</pre>
|-
|
# ''Numbered lists'' are:
## Very organized
## Easy to follow
A new line marks the end of the list.
# New numbering starts with 1.

|<pre>
# ''Numbered lists'' are:
## Very organized
## Easy to follow
A new line marks the end of the list.
# New numbering starts with 1.
</pre>
|-
|
: A colon (:) indents a line or paragraph.
A newline starts a new paragraph. <br>
Often used for discussion on talk pages.
: We use 1 colon to indent once.
:: We use 2 colons to indent twice.
::: 3 colons to indent 3 times, and so on.
|<pre>
: A colon (:) indents a line or paragraph.
A newline starts a new paragraph. <br>
Often used for discussion on talk pages.
: We use 1 colon to indent once.
:: We use 2 colons to indent twice.
::: 3 colons to indent 3 times, and so on.
</pre>


|-
|
Here's a link to the [[Main page]].

But be careful - capitalization counts!  
|<pre>
Here's a link to the [[Main page]].
</pre>
|-
|
[[Hams with two Heads]] is a page that doesn't exist
yet. You could create it by clicking on the link.
|<pre>
[[Hams with two Heads]] is 
a page that doesn't exist
yet. You could create it by 
clicking on the link.
</pre>
|-
|
You can link to a page section by its title:

* [[List of hams with two heads]].

If multiple sections have the same title, add
a number. [[#Example section 3]] goes to the
third section named "Example section".
|<pre>

You can link to a page section by its title:

* [[List hams with two heads #VK4YEH]].

If multiple sections have the same title, add
a number. [[#Example section 3]] goes to the
third section named "Example section".
</pre>
|}

==Converting Word Documents to use on this wiki==
Saving a relatively simple Word document (no images or tables) to html and then running that through the converter [http://diberri.dyndns.org/html2wiki.html here] produced good mediawiki formatting. A document including images, tables, and centered text did not work as well.  The images would need to be added to OWW separately, the table also didn't come out quite right and centered text was no longer centered.

A direct converter can be found [http://216.239.37.104/translate_c?u=http://www.homeopathy.at/wiki/index.php%3Ftitle%3DWord2Wiki here].

A series of Word macros for doing simple conversions (including tables) is [http://www.infpro.com/Word2MediaWiki.aspx here]; they seem to work reasonably well but aren't designed for sophisticated layouts.

Also, with the release of [http://www.openoffice.org/ OpenOffice],  2.4, OpenOffice can now export documents to mediawiki format. Since OpenOffice can also read MS Word documents, this allows OpenOffice to serve as a Word to MediaWiki converted.

==converting Images for use in this wiki - for mac computers==

Users have had good success with the following steps for porting images embeddded in word documents to MediaWiki format on a Mac:
# Click on the image in the word document and choose Edit->Copy from the menu (cmd-C)
# Go to the application GraphicConverter and choose File->New->Image with clipboard (cmd-J)
# Choose File->Save as and save as a JPEG/JFIF format (.jpg) file with 100% Quality.

Alternatively, if you want to take an image which has associated text boxes, it seems to come out well if you take a screenshot of a selection with Grab (in the /Applications/Utilities folder), save as a .tiff (your only option) and then open in GraphicConverter and save as a JPEG as described above.


== Adding Excel spreadsheets to this wiki ==

Procedure:
* Sort your spreadsheet to display what you wish to display
* Save the spreadsheet as a CSV file
* Go to this site [http://area23.brightbyte.de/csv2wp.php CSV Converter] or search for another compatible site that will convert CSV to WikiMedia format
* Upload the CSV file into the converter
* Ensure that the "Separator Character" is set to '''Other''' and type in the pipe character '''|'''
* In the "Convert Linebreaks in Cells" click on '''Replace with space'''
* Click on "'''Convert to MediaWiki'''"
* Copy and paste to the wiki page

==Converting HTML to Wiki markup==

A number of online markup converters are available. [http://www.uni-bonn.de/~manfear/html2wiki-tables.php#wiki This one] has been used successfully to convert HTML tables and text to WIKI markup.

==Converting PDF files==

PDF files will probably need to be converted to some other format eg doc or xls before converting to wiki markup.  [http://www.freepdfconvert.com/convert_pdf_to_source.asp This site] will do that providing that you know the password to the file if it is protected.

== Adding Mathematical formulae to this wiki ==

Please  click on this link: [[Mathematics on this wiki]] to go to another page. The information on it was copied from the mediawiki site and modified to suit this site.

== Uploading files to this wiki ==

'''Please Note - Restrict your file size to 15kB'''  This will speed up page rendering!

At the moment, only graphics file upload ais enable on this wiki. Currently the file-types that will be accepted by the server are .jpg  .png and  .ogg  .   These are stored in the wiki database. Should there be enough demand, it is possible to enable other file types.

'''Guidelines''': When naming your files for upload, please use the following syntax yourusername_filename.extension  for example VK4YEH_radio1.jpg

This will streamline uploads and ensure that files with the same name are unlikely to occur.

When linking files on pages use the following syntax [[image:vk4yeh_image_file_syntax.jpg|420px]], 

For example this code [[Image:image_file_example_syntax.jpg|450px]]

adds the following image to the text.
  [[ image:Vk4yeh_0_VK4YEH.jpg]]

Please click on "edit' to check the syntax if you need to.

== Adding your uploaded images to your text ==

Typically you would '''upload an image file''' to the wiki   ...''before'' you use the following wiki text to place the image on an article:  

{| 
|-
||'''Description'''
||'''You type''' 
||'''You get'''
|-
|Embed image<br /> (with alt text)
|
<code><nowiki>[[Image:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]</nowiki></code>
|
[[Image:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]
|-
|Link to description page
|
<code><nowiki>[[:image:Vk4yeh_0_VK4YEH.jpg]]</nowiki></code><br />
<code><nowiki>[[:image:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]</nowiki></code>
|
[[:image:Vk4yeh_0_VK4YEH.jpg]]<br />
[[:image:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]
|-
|Link directly to file
|
<code><nowiki>[[Media:Vk4yeh_0_VK4YEH.jpg]]</nowiki></code><br />
<code><nowiki>[[Media:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]</nowiki></code>
|
[[Media:Vk4yeh_0_VK4YEH.jpg]]<br />
[[Media:Vk4yeh_0_VK4YEH.jpg|VK4YEH]]
|-
|Thumbnail<br /> (centered, 100 pixels<br /> wide, with caption)
|
<code><nowiki>[[image:Vk4yeh_0_VK4YEH.jpg|center|thumb|100px|VK4YEH]]</nowiki></code>
|
[[image:Vk4yeh_0_VK4YEH.jpg|center|thumb|100px|VK4YEH]]
|-
|Border<br /> (100 pixels) <br /> Results in a very small gray border
<!-- Note: because the fine gray border can not be seen when used on the "VK4YEH"-image an image is used with a white background -->
|
<code><nowiki>[[image:Vk4yeh_0_VK4YEH.jpg|border|100px]]</nowiki></code>
|
[[Image:Vk4yeh_0_VK4YEH.jpg|border|100px]]
|-
|Frameless<br />Like thumbnail, respect user preferences for image width but without border and no right float.
|
<code><nowiki>[[Image:Vk4yeh_0_VK4YEH.jpg|frameless]]</nowiki></code>
|
[[Image:Vk4yeh_0_VK4YEH.jpg|frameless]]
|}

=== Syntax ===
The full syntax for displaying an image is:
 <code><nowiki>[[Image:{name}|{options}]]</nowiki></code>
Where options can be zero or more of the following, separated by pipes:
*<code>thumb</code>, <code>thumbnail</code>, or <code>frame</code>: Controls how the image is formatted
*<code>left</code>, <code>right</code>, <code>center</code>, <code>none</code>: Controls the alignment of the image on the page
*<code>{width}px</code>: Resizes the image to the given width in pixels
*<code>{caption text}</code>
* Special cases:
** <code>page=1</code>: displays the specified page when showing a djvu file.

The options can be given in any order. If a given option does not match any of the other possibilities, it is assumed to be the caption text. Caption text can contain wiki links or other formatting.

=== Other files ===
You can link to an external file using the same syntax used for linking to an external web page. 
*<code><nowiki>[http://url.for/some/image.png]</nowiki></code>
Or with different text:
*<code><nowiki>[http://url.for/some/image.png link text here]</nowiki></code>

This is enabled on this wiki, so you can also embed external images. To do that, simply insert the image's url:
*<code><nowiki>http://url.for/some/image.png</nowiki></code>

=== Gallery of images ===
It's easy to make a gallery of thumbnails with the <code><nowiki><gallery></nowiki></code> tag. The syntax is:
<pre>
<gallery>
Image:{filename}|{caption}
Image:{filename}|{caption}
{...}
</gallery>
</pre>
Captions are optional, and may contain wiki links or other formatting.

for example:
<pre>
<gallery>
Image:Vk4yeh_0_VK4YEH.jpg|Item 1
Image:Vk4yeh_0_VK4YEH.jpg[[Help:Contents]]
Image:Vk4yeh_0_VK4YEH.jpg
Image:Vk4yeh_0_VK4YEH.jpg
Image:Vk4yeh_0_VK4YEH.jpg|''italic caption''
</gallery>
</pre>
is formatted as:
<gallery>
Image:Vk4yeh_0_VK4YEH.jpg|Item 1
Image:Vk4yeh_0_VK4YEH.jpg|a link to [[Help:Contents]]
Image:Vk4yeh_0_VK4YEH.jpg
Image:Vk4yeh_0_VK4YEH.jpg
Image:Vk4yeh_0_VK4YEH.jpg|''italic caption''
</gallery>

== Adding a table to a wiki page ==

===using html===

The simplest way to add a table is to use HTML coding. This table:

<table border=1>
<tr>
<td>Heading 1</td>
<td>Heading 2</td>
<td>Heading 3<td>
</tr>
<tr>
<td>row 1 cell 1</td>
<td>row 1 cell 2</td>
<td>row 1 cell 3</td>
</tr>
<tr>
<td>row 2 cell 1</td>
<td>row 2 cell 2</td>
<td>row 2 cell 3</td>
</tr>
<tr>
<td>row 3 cell 1</td>
<td>row 3 cell 2</td>
<td>row 3 cell 3</td>
</tr>
</table>

is created by this html code:

[[Image:vk4yeh_simple_table.jpg|420px]]

=== using wiki markup ===

Below is wiki markup for a simple table, and the corresponding table created by it:

{|style = width:55" border = "1"
|Heading 1||Heading 2||Heading 3 
|---- 
|row 1 cell 1||row 1 cell 2||row  cell 3 
|---- 
|row 2 cell 1||row 2 cell2||row 2 cell3 
|---- 
|row 3 cell 1||row 3 cell 2||row 3 cell3 
|---- 
|---- |}
 

[[Image:vk4yeh_wiki_markup_simple_table.jpg|450px|left]]
