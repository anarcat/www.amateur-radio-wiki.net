<div style="border-left: 1em solid white; float: right; clear: right; margin-left: 1em;">
<div class="infobox" style="width: 330px; text-align: left; font-size: 90%; float: none; margin: 0px;">
<div style="text-align:center; font-size: 120%;">'''{{PAGENAME}}'''</div>
<div id="app-infobox-image" style="text-align:center;">[[Image:{{{Image|{{PAGENAME}}.png}}}|320px]]<br />{{#ifexist: Image:{{{Image|{{PAGENAME}}.png}}}||''There is no image of this program in action — <span class="plainlinks">[{{fullurl:Special:Upload|wpDestFile={{{Image|{{PAGENAMEE}}.png}}}}} upload one]</span>!''<includeonly>[[Category:Need pictures]]</includeonly>}}</div>
<table>
<tr><th style="min-width: 12em;">Application type</th><td style="vertical-align: middle;">[[Software Defined Radio (SDR)]]</td></tr>
<tr><th>Cost</th><td style="vertical-align: middle;">{{{Cost|''Unknown''}}}</td></tr>
<tr><th>Platforms</th><td style="vertical-align: middle;">{{{Platforms|''Unknown''}}}</td></tr>
<tr><th>Reception modes</th><td style="vertical-align: middle;">{{{ReceptionModes|''Unknown''}}}</td></tr>
<tr><th>Transmission modes</th><td style="vertical-align: middle;">{{{TransmissionModes|''Unknown''}}}</td></tr>
<tr><th>Max demod bandwidth</th><td style="vertical-align: middle;">{{{MaxBandwidth|''Unknown''}}}</td></tr>
<tr><th>Supported devices</th><td style="vertical-align: middle;">{{{Devices|''Unknown''}}}</td></tr>
<tr><th>Homepage</th><td style="vertical-align: middle;">{{{Homepage|''Unknown''}}}</td></tr>
</table>
</div></div><noinclude> <!-- do not put any newlines here otherwise it puts paragraph markers after the infobox on inclusion pages -->
<i>This template is used to display an information box containing details about a software program for working with software-defined radio. See [[HDSDR]] for example use.</i>

Valid parameters and examples are:
{|class="wikitable"
! Parameter/example !! Purpose !! Valid values
|-
| Platforms = Windows, Mac, Linux || Which platforms this application runs on || Free text
|-
| Cost = Free || Purchase price of the program || List approximate price or '''Free''' if the program is noncommercial
|-
| ReceptionModes = <nowiki>[[AM]], [[FM]]</nowiki> || What type of transmissions can the program receive? || Any transmission mode.  Try to link to existing wiki pages where possible
|-
| TransmissionModes = <nowiki>'''None'''</nowiki> || What type of modulations can be transmitted with this program? || Same list as for ReceptionModes, or '''None''' if the program is receive-only
|-
| MaxBandwidth = 96kHz || Maximum bandwidth that can be demodulated (e.g. 96kHz is not quite enough to fully demodulate a [[WBFM]] signal, so a program that can only demodulate up to 96kHz cannot be used to reliably listen to consumer FM radio) || A value or <nowiki>'''Unlimited'''</nowiki>
|-
| Devices = <nowiki>[[RTL2832]]</nowiki> || What devices can this program work with? || List of SDR devices.  Some common examples: <ul><li>'''<nowiki>[[Stereo sound card]]</nowiki>''' (any radio connected to an internal or USB sound card)</li><li>'''<nowiki>[[SDR capture file]]</nowiki>''' (if the program can record data to a file on disk for later examination)</li></ul>
|-
| Homepage = ''URL'' || Address of the program's homepage || Any URL
|}
Default values are shown if the parameter is omitted, so do not use any parameters unless the value is known (most of the defaults say "unknown")
[[Category:Infoboxes]]
</noinclude><includeonly>
[[Category:Programs for working with SDR]]
</includeonly>
